��    V      �     |      x      y  &   �     �  1   �       !   '     I     _     d     k     �     �     �     �     �     �     �     �     
	     	     (	     5	     C	     Z	     q	  %   �	     �	  (   �	     �	     �	     
     
     )
     8
     I
     ^
     t
     �
     �
     �
     �
     �
     �
  	   �
  *   �
     '     ,     3     6  &   ;     b     r     �     �     �     �  "   �     �  	             #     /  
   =  	   H  &   R  .   y     �     �     �     �     �               *  
   6  	   A     K     O     V     i     w     }     �  
   �     �  �  �  "   X  +   {     �  6   �  (   �           >     X  
   a     l     �     �     �     �     �     �     �          $     4     F  .   `     �     �     �  9   �        1        Q     e     ~     �     �     �     �     �     �     �               :     H     V  
   k  9   v     �     �     �     �  '   �     �     �                )  %   <  %   b     �     �     �  	   �     �     �  
   �  '   �  ,        E     ]     t     �     �     �     �     �     �            
        #     3     E     L     Z     y     �                   ,       2   M   S   H              4          A   E       C          V         <       T           &   (          7   N                    -      *          5   '                 "   )   K          ?   	          
       J   F           B          .   1   >      U                 @   3               6           +      /   %   I           8   P   =       $                         #   R   L      :   G              Q          D       !          9   0   O   ;    <big><b>Argument Error</b></big> <big><b>File Not Found Error</b></big> <big><b>Installing  <big><b>Invalid Flatpak Reference Error</b></big> <big><b>Running Error</b></big> <big><b>Selection Error</b></big> <big><b>Uninstalling  Arch Arch:  Are you sure to install  Are you sure to uninstall  Branch Branch:  Click here for search Collection ID:  Commit:  Copyright (C) 2020 Erdem Ersoy Deploy Dir:  Download Size Download Size:  EOL Reason:  EOL Rebased:  Error at installation! Error at uninstalling! Error at updating! Error reading About dialog GUI file:  Error reading GUI file:  Error reading message dialogs GUI file:  File not found:  Flatpak GUI for Pardus I_nstall Info About  Installed Size Installed Size:  Installing canceled! Installing completed! Installing from file... Installing... Installing:  Invalid Flatpak reference is:  Is Current:  Is Installed:  Latest Commit:  License:  Manage Flatpak softwares via GUI on Pardus Name Name:  No None None of the applications are selected. Not installed:  Not uninstalled:  Not updated:  Origin:  Pardus Flatpak GUI Pardus Flatpak GUI Error Dialog Pardus Flatpak GUI Question Dialog Pardus Flatpak GUI Web Site Real Name Real Name:  Remote Name Remote Name:  Subpaths:  Summary:  The selected application couldn't run. There are too many arguments. Argument count:  Uninstalling canceled! Uninstalling completed! Uninstalling... Uninstalling:  Updating All Updating canceled! Updating completed! Updating... Updating:  Version:  Yes _About _Copy to Clipboard _FlatHub Page _Info _Run _Show Installed Apps _Uninstall _Update All Project-Id-Version: Pardus Flatpak GUI 0.4.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-18 10:34+0300
Last-Translator: Erdem Ersoy <<erdem.ersoy@pardus.org.tr>>
Language-Team: Erdem Errsoy <erdem.ersoy@pardus.org.tr>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
 <big><b>Argüman Hatası</b></big> <big><b>Dosya Bulunamadı Hatası</b></big> <big><b>Yükleniyor:  <big><b>Geçersiz Flatpak Başvurusu Hatası</b></big> <big><b>Çalıştırma Hatası</b></big> <big><b>Seçim Hatası</b></big> <big><b>Kaldırılıyor:  Mimarisi Mimarisi:  Yüklemeye emin misiniz:  Kaldırmaya emin misiniz:  Dalı Dalı:  Aramak için tıklayınız Koleksiyon Kimliği:  Değişiklik:  Telif Hakkı: 2020 Erdem Ersoy Yerleştiği Dizini:  İndirme Boyutu İndirme Boyutu:  Süresinin Bitme Nedeni:  Süresinin Bitme Yeniden Tanımlanmış Adı:  Yüklemede hata! Kaldırmada hata! Güncellemede hata! Hakkında iletişim kutusu GUI dosyasını okumada hata:  GUI dosyasını okumada hata:  İleti pencereleri GUI dosyasını okumada hata:  Dosya bulunamadı:  Pardus için Flatpak GUI _Yükle Hakkında Bilgi:  Yüklü Boyutu Yükleme Boyutu:  Yükleme iptal edildi! Yükleme tamamlandı! Dosyadan yükleniyor... Yükleniyor... Yükleniyor:  Geçersiz Flatpak başvurusu:  Şimdiki mi:  Yüklü mü:  Son Değişikliği:  Lisansı:  Pardus üzerinde GUI ile Flatpak yazılımlarını yönet Adı Adı:  Hayır Yok Uygulamaların hiçbiri seçili değil. Yüklenmedi:  Kaldırılmadı:  Güncellenmedi:  Köken:  Pardus Flatpak GUI Pardus Flatpak GUI Hata İleti Kutusu Pardus Flatpak GUI Soru İleti Kutusu Pardus Flatpak GUI Ağ Sitesi Gerçek Adı Gerçek Adı:  Uzak Adı Uzak Adı:  Alt Yollar:  Tanımı:  Seçili uygulama çalıştırılamadı. Çok fazla argüman var. Argüman sayısı:  Kaldırma iptal edildi! Kaldırma tamamlandı! Kaldırılıyor... Kaldırılıyor:  Tümünü Güncelleme Güncellenme iptal edildi! Güncellenme tamamlandı! Güncelleniyor... Güncelleniyor:  Sürümü:  Evet _Hakkında _Panoya Kopyala _FlatHub Sayfası _Bilgi Ç_alıştır _Yüklü Uygulamaları Göster Ka_ldır _Tümünü Güncelle 